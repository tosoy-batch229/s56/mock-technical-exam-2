function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    // Check if the letter parameter is a single letter.
    if (letter.length != 1) {
        // If it is, return 'undefined'
        return undefined;
    } else {
        // Check if how many times the letter occur.
        for (let i = 0; i < sentence.length; i++) {
            if (letter === sentence[i]) {
                result++;
            }
        }
        // Return the final count.
        return result;
    }
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // Convert the text into lowercase.
    text = text.toLowerCase();
    // For storing the existing letters.
    const existingLetters = {};

    for (let i = 0; i < text.length; i++) {
        // Check if the letter is already inside the exitingLetters.
        if (existingLetters[text[i]]) {
            // If it is, return false.
            return false;
        }
        // If it is not, add the letter to the existingLetters.
        existingLetters[text[i]] = 1;
    }
    // If there are no same letters, return true.
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let result;
    if (age < 13) {
        // Return 'undefined' for customers under 13.
        result = undefined;
    }
    // Return the discounted price for students aged 13 to 21 and senior citizens aged above 70 (20% discount)
    if (age >= 13 && age <= 21) {
        result = (price * 0.8).toFixed(2);
    }
    if (age > 70) {
        result = (price * 0.8).toFixed(2);
    }
    // Return the original price for people aged 22 to 64
    if (age >= 22 && age <= 64) {
        result = price.toFixed(2);
    }
    return result;
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].

    const noStocks = [];
    for (let i = 0; i < items.length; i++) {
        // Check if the stocks property is equal to zero.
        if (items[i].stocks === 0) {
            // Check if the category is alreay in the noStocks array.
            let active = items[i].category;
            if (noStocks.indexOf(active) === -1) {
                noStocks.push(active);
            }
        }
    }
    return noStocks;

    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    let result = [];
    // Loop through the candidateA array.
    for (let i = 0; i < candidateA.length; i++) {
        // Check the element is included in the candidateB array.
        if (candidateB.includes(candidateA[i])) {
            // If it is, push it to that.
            result.push(candidateA[i]);
        }
    }
    // Return the array containing the same elements of both arrays.
    return result;

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};